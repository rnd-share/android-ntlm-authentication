# Android NTLM Authentication


Link Reference :

Android - NTML : https://stackoverflow.com/questions/8770581/ntlm-authentication-in-android
https://www.example-code.com/android/http_authentication.asp

You can try connect website using ntml authentication

http://browserspy.dk/password.php
http://browserspy.dk/password-ok.php

thanks : https://serverfault.com/questions/595035/testing-ntlm-kerberos-against-a-public-url


Library 
okhttpclient : https://www.journaldev.com/13629/okhttp-android-example-tutorial
jcifs : https://www.jcifs.org/


dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'com.android.support:appcompat-v7:28.0.0'
    implementation 'com.android.support.constraint:constraint-layout:1.1.3'
    implementation group: 'com.squareup.okhttp3', name: 'okhttp', version: '3.12.0'
    
    // https://mvnrepository.com/artifact/jcifs/jcifs
    implementation  group: 'jcifs', name: 'jcifs', version: '1.3.17'

    // https://mvnrepository.com/artifact/org.codelibs/jcifs
    //implementation  group: 'org.codelibs', name: 'jcifs', version: '2.1.3'
}

Thank you :)