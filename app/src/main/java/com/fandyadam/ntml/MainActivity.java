package com.fandyadam.ntml;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chilkatsoft.CkHttp;
import com.chilkatsoft.CkJsonObject;
import com.chilkatsoft.CkNtlm;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    static {
        // Important: Make sure the name passed to loadLibrary matches the shared library
        // found in your project's libs/armeabi directory.
        //  for "libchilkat.so", pass "chilkat" to loadLibrary
        //  for "libchilkatemail.so", pass "chilkatemail" to loadLibrary
        //  etc.
        //
        try {
            System.loadLibrary("chilkat");
        } catch (UnsatisfiedLinkError e) {
            System.err.println("Native code library failed to load.\n" + e);
            System.exit(1);
        }

        // Note: If the incorrect library name is passed to System.loadLibrary,
        // then you will see the following error message at application startup:
        //"The application <your-application-name> has stopped unexpectedly. Please try again."
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    run();
                } catch (Exception e) {
                    System.out.println("Exception : " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Exception : " + e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }

    public void chilkat() {
//        CkNtlm ckNtlm = new CkNtlm();
//        ckNtlm.UnlockComponent("Start my 30-day Trial");
//        ckNtlm.put_Password("test");
//        ckNtlm.put_UserName("test");
//
//        CkHttp ckHttp = new CkHttp();
//        ckHttp.UnlockComponent("Start my 30-day Trial");
//        ckHttp.put_NtlmAuth(true);
//        ckHttp.put_LoginDomain("test");
//        ckHttp.put_ProxyPassword("test");
//        ckHttp.domain("BCA");
//
//        String html = ckHttp.quickGetStr("http://browserspy.dk/password-ok.php");
//        boolean status = ckHttp.get_LastMethodSuccess();
//        //  Note:
//        if (ckHttp.get_LastMethodSuccess() != true) {
//            Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_SHORT).show();
//        }
//
//        System.out.println(ckHttp.lastErrorText());
//        Toast.makeText(getApplicationContext(), "Response : "+status+" | " + html, Toast.LENGTH_LONG).show();


        String TAG = "Chilkat";

        CkNtlm ntlmClient = new CkNtlm();

        boolean success = ntlmClient.UnlockComponent("Anything for 30-day trial");
        if (success != true) {
            Log.i(TAG, ntlmClient.lastErrorText());
            return;
        }

        //  UnlockComponent only needs to be called once on the 1st object instance.

        CkNtlm ntlmServer = new CkNtlm();

        //  The NTLM protocol begins by the client sending the server
        //  a Type1 message.
        String type1Msg;
        ntlmClient.put_Workstation("MyWorkstation");
        type1Msg = ntlmClient.genType1();

        Log.i(TAG, "Type1 message from client to server:");
        Log.i(TAG, type1Msg);

        //  If the server wishes to examine the information embedded within the
        //  Type1 message, it may call ParseType1.
        //  This step is not necessary, it is only for informational purposes..
        String type1Info = ntlmServer.parseType1(type1Msg);

        Log.i(TAG, "---");
        Log.i(TAG, type1Info);

        //  The server now generates a Type2 message to be sent to the client.
        //  The Type2 message requires a TargetName.  A TargetName is
        //  the authentication realm in which the authenticating account
        //  has membership (a domain name for domain accounts, or server name
        //  for local machine accounts).
        ntlmServer.put_TargetName("myAuthRealm");

        String type2Msg = ntlmServer.genType2(type1Msg);
        if (ntlmServer.get_LastMethodSuccess() != true) {
            Log.i(TAG, ntlmServer.lastErrorText());
            return;
        }

        Log.i(TAG, "Type2 message from server to client:");
        Log.i(TAG, type2Msg);

        //  The client may examine the information embedded in the Type2 message
        //  by calling ParseType2, which returns XML.  This is only for informational purposes
        //  and is not required.
        String type2Info = ntlmClient.parseType2(type2Msg);

        Log.i(TAG, "---");
        Log.i(TAG, type2Info);

        //  The client will now generate the final Type3 message to be sent to the server.
        //  This requires the Username and Password:
        ntlmClient.put_UserName("test123");
        ntlmClient.put_Password("myPassword");

        String type3Msg;
        type3Msg = ntlmClient.genType3(type2Msg);
        if (ntlmClient.get_LastMethodSuccess() != true) {
            Log.i(TAG, ntlmClient.lastErrorText());
            return;
        }

        Log.i(TAG, "Type3 message from client to server:");
        Log.i(TAG, type3Msg);

        //  The server may verify the response by first "loading" the Type3 message.
        //  This sets the various properties such as Username, Domain, Workstation,
        //  and ClientChallenge to the values embedded within theType3 message.
        //  The server may then use the Username to lookup the password.
        //  Looking up the password is dependent on your infrastructure.  Perhaps your
        //  usernames/passwords are stored in a secure database.  If that's the case, you would
        //  write code to issue a query to get the password string for the given username.
        //  Once the password is obtained, set the Password property and then
        //  generate the Type3 response again.  If the server's Type3 response matches
        //  the client's Type3 response, then the client's password is correct.

        success = ntlmServer.LoadType3(type3Msg);
        if (success != true) {
            Log.i(TAG, ntlmServer.lastErrorText());
            return;
        }

        //  The Username property now contains the username that was embedded within
        //  the Type3 message.  It can be used to lookup the password.
        String clientUsername = ntlmServer.userName();

        //  For this example, we'll simply set the password to a literal string:
        ntlmServer.put_Password("myPassword");

        //  The server may generate the Type3 message again, using the client's correct
        //  password:
        String expectedType3Msg = ntlmServer.genType3(type2Msg);

        Log.i(TAG, "Expected Type3 Message:");
        Log.i(TAG, expectedType3Msg);

        //  If the Type3 message received from the client is exactly the same as the
        //  expected Type3 message, then the client must've used the same password,
        //  and authentication is successful.

    }

    public void setImage(Bitmap bm) {
        ImageView imageView = findViewById(R.id.imageView);
        imageView.setImageBitmap(bm);
    }

    public void run() throws IOException {

        EditText textUsername = findViewById(R.id.username);
        EditText textPassword = findViewById(R.id.password);
        EditText textDomain = findViewById(R.id.domain);
        EditText textUrl = findViewById(R.id.url);

        String username = textUsername.getText().toString();
        String password = textPassword.getText().toString();
        String domain = textDomain.getText().toString();
        String url = textUrl.getText().toString();

        System.out.println("username : " + username);
        System.out.println("password : " + password);
        System.out.println("domain   : " + domain);
        System.out.println("url      : " + url);

        OkHttpClient client = new OkHttpClient.Builder()
                .authenticator(new NTLMAuthenticator(username,
                        password,
                        domain,
                        ""))
//                .authenticator(new NTLMAuthenticator("test", "test1", "DOMAIN", ""))
                .build();

        Request request = new Request.Builder()
                .url(url)
//                .url("http://browserspy.dk/password-ok.php")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, final IOException e) {
                System.out.println("Failure : " + e.getMessage());

                final String message = e.getMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Toast.makeText(getApplicationContext(), "Failure : " +message, Toast.LENGTH_LONG).show();
                        } catch (Exception ex) {
                            System.out.println("Failure : " + e.getMessage());
                            Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_LONG).show();
                        }
                        call.cancel();
                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                System.out.println("Response : ");

                if (response.isSuccessful()) {

                    InputStream inputStream = response.body().byteStream();
                    final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                    final String myResponse = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            System.out.println("Response : " + myResponse);

                            setImage(bitmap);

                            Toast.makeText(getApplicationContext(), "Response : " + myResponse, Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });
    }

}
